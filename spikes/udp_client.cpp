#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h> 
#include <string.h> 
#include <sys/types.h> 
#include <sys/socket.h> 
#include <arpa/inet.h> 
#include <netinet/in.h> 
#include <iostream>
#include <cstring>
  
#define PORT        8080
#define MAXLINE     1024
#define SERVER_IP   "localhost"
  
// Driver code 
int main() { 
    int sockfd;
    char buffer[MAXLINE];
    char *hello = "Hello from client";
    struct sockaddr_in     servaddr;
  
    // Creating socket file descriptor 
    if ( (sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) { 
        perror("socket creation failed");
        exit(EXIT_FAILURE);
    }
  
    memset(&servaddr, 0, sizeof(servaddr));
    inet_pton(AF_INET, SERVER_IP, &(servaddr.sin_addr));
      
    // Filling server information 
    servaddr.sin_family = AF_INET;
    servaddr.sin_port = htons(PORT);
    // servaddr.sin_addr.s_addr = INADDR_ANY;
    std::string msg = hello;
    socklen_t n, len;
    while (true) {
        sendto(sockfd, msg.c_str(), msg.length(), 
            MSG_CONFIRM, (const struct sockaddr *) &servaddr,  
                sizeof(servaddr));
        printf("Hello message sent.\n");
        std::cin >> msg;
            
        // n = recvfrom(sockfd, (char *)buffer, MAXLINE,  
        //             MSG_WAITALL, (struct sockaddr *) &servaddr, 
        //             &len);
        // buffer[n] = '\0';
        // printf("Server : %s\n", buffer);
        // usleep(500000);
    }
  
    close(sockfd);
    return 0;
}
