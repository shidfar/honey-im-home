#ifndef HIH_DATA_TRANSFER_H
    #define HIH_DATA_TRANSFER_H

    #include <iostream>
    #include <cstring>
    #include <mutex>

    #include "GlobalDefs.h"

        namespace honeyhome {
            class DataTransfer {
            private:
                void * transfer_buffer;
                uint32_t transfer_size = 0;

                std::mutex guard;
                volatile bool stale{true};

            public:
                void write_data(void * buff, uint32_t size) {
                    {
                        const std::lock_guard<std::mutex> lock(this->guard);
                        if (this->transfer_size > 0) free(this->transfer_buffer);
                        this->transfer_buffer = malloc(size);
                        this->transfer_size = size;
                        memcpy(this->transfer_buffer, buff, this->transfer_size);
                        this->stale = false;
                    }
                }

                uint32_t read_data(void **buff) {
                    {
                        const std::lock_guard<std::mutex> lock(this->guard);
                        if (this->transfer_size < 1) return this->transfer_size;
                        *buff = malloc(this->transfer_size);
                        memcpy(*buff, this->transfer_buffer, this->transfer_size);
                        this->stale = true;
                        return this->transfer_size;
                    }
                }

                bool is_stale() {
                    const std::lock_guard<std::mutex> lock(this->guard);
                    return stale;
                }
            };
        }

#endif