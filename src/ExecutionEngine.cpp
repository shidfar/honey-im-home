#include "ExecutionEngine.hpp"

namespace honeyhome {

    ExecutionEngine::ExecutionEngine() {
        this->context = new HihContext(F_HEIGHT, F_WIDTH);
        this->incoming_data = new DataTransfer();
        this->outgoing_data = new DataTransfer();
        this->video_camera = new VideoCamera(context, outgoing_data);
        this->window = new Window(context, incoming_data);
        this->udp_server = new UdpServer(context, incoming_data);
        this->udp_client = new UdpClient(context, outgoing_data);
    }
    
    ExecutionEngine::~ExecutionEngine() {
        std::cout << "Destructing the engine" << std::endl;
        delete video_camera;
        delete window;
        delete context;
        delete outgoing_data;
        delete incoming_data;
    }

    bool ExecutionEngine::is_running() {
        return !this->context->quit;
    }

    void ExecutionEngine::start() {
        std::cout << "Starting the engine" << std::endl;
        if (video_camera->initialize() == 0 && udp_server->initialize() && udp_client->initialize()) {
            std::cout << "Successfully initialized camera" << std::endl;
            this->camera_thread = std::thread(&ExecutionEngine::start_camera_thread, this);
            this->ui_thread = std::thread(&ExecutionEngine::start_ui_thread, this);
            this->event_thread = std::thread(&ExecutionEngine::start_event_thread, this);
            this->udp_server_thread = std::thread(&ExecutionEngine::start_udp_server_thread, this);
            this->udp_client_thread = std::thread(&ExecutionEngine::start_udp_client_thread, this);
            
            if (this->camera_thread.joinable()) this->camera_thread.join();
            else std::cout << "t1 is not joinable" << std::endl;
            
            if (this->ui_thread.joinable()) this->ui_thread.join();
            else std::cout << "t2 is not joinable" << std::endl;

            if (this->event_thread.joinable()) this->event_thread.join();
            else std::cout << "t3 is not joinable" << std::endl;

            if (this->udp_server_thread.joinable()) this->udp_server_thread.join();
            else std::cout << "t4 is not joinable" << std::endl;

            if (this->udp_client_thread.joinable()) this->udp_client_thread.join();
            else std::cout << "t5 is not joinable" << std::endl;
        }
    }

    void ExecutionEngine::start_ui_thread() {
        std::cout << "Starting ui_thread" << std::endl;
        window->draw();
        std::cout << "Finished ui_thread" << std::endl;
    }
    
    void ExecutionEngine::start_event_thread() {
        std::cout << "Starting event_thread" << std::endl;
        window->start_event_loop();
        std::cout << "Finished event_thread" << std::endl;
    }
    
    void ExecutionEngine::start_camera_thread() {
        std::cout << "Starting camera_thread" << std::endl;
        video_camera->start_streaming();
        std::cout << "Finished camera_thread" << std::endl;
    }
    
    void ExecutionEngine::start_udp_server_thread() {
        std::cout << "Starting udp_server_thread" << std::endl;
        udp_server->reciever_loop();
        std::cout << "Finished udp_server_thread" << std::endl;
    }

    void ExecutionEngine::start_udp_client_thread() {
        std::cout << "Starting udp_client_thread" << std::endl;
        udp_client->sender_loop();
        std::cout << "Finished udp_client_thread" << std::endl;
    }

    void ExecutionEngine::start_juggling() {
        // only for local debugging purposes
        juggler->juggle();
    }
}
