#ifndef HIH_EXECUTION_ENGINE_H
    #define HIH_EXECUTION_ENGINE_H

    #include "VideoCamera.hpp"
    #include "DataTransfer.hpp"
    #include "HihContext.hpp"
    #include "Window.hpp"
    #include "UdpServer.hpp"
    #include "UdpClient.hpp"
    #include "GlobalDefs.h"
    #include "Juggler.hpp"

    namespace honeyhome {
        class ExecutionEngine
        {
        private:
            DataTransfer *incoming_data;
            DataTransfer *outgoing_data;
            HihContext *context;
            VideoCamera *video_camera;
            Window *window;
            UdpServer *udp_server;
            UdpClient *udp_client;
            Juggler *juggler;

            std::thread ui_thread;
            std::thread event_thread;
            std::thread camera_thread;
            std::thread udp_server_thread;
            std::thread udp_client_thread;
            std::thread juggler_thread;

            void start_ui_thread();
            void start_event_thread();
            void start_camera_thread();
            void start_udp_server_thread();
            void start_udp_client_thread();
            void start_juggling();

        public:
            ExecutionEngine();
            ~ExecutionEngine();
            
            void start();
            bool is_running();
        };
    }
    

#endif