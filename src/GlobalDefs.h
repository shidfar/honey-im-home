#ifndef HIH_GLOBAL_DEFS
    #define HIH_GLOBAL_DEFS

    #define FPS_30 33333
    #define FPS_50 20000
    #define FPS_60 16667
    #define FPS_24 41667
    #define W_WIDTH 1280
    #define W_HEIGHT 960
    #define F_WIDTH 160 // 320
    #define F_HEIGHT 120 // 180
    #define WINDOW_NAME "Honey I'm Home"
    #define UDP_PAYLOAD_SIZE 65400
    #define UDP_SERVER_PORT 8080
    #define UDP_SERVER_HOST "127.0.0.1"
    #define REMOTE_UDP_HOST "127.0.0.1"
//  | height: 480, width: 640 requires: 900 KB per frame
//  | height: 120, width: 160 requires: 56.25 KB per frame
//  | height: 144, width: 176 requires: 74.25 KB per frame
//  | height: 180, width: 320 requires: 168.75 KB per frame
//  | height: 240, width: 320 requires: 225 KB per frame
//  | height: 288, width: 352 requires: 297 KB per frame
//  | height: 240, width: 424 requires: 298.125 KB per frame
//  | height: 360, width: 640 requires: 675 KB per frame
//  | height: 480, width: 848 requires: 1192.5 KB per frame
//  | height: 540, width: 960 requires: 1518.75 KB per frame
//  | height: 720, width: 1280 requires: 2700 KB per frame

#endif
