#ifndef HIH_CONTEXT_H
    #define HIH_CONTEXT_H
    
    #include <iostream>
    #include <linux/videodev2.h>
    #include <mutex>

    namespace honeyhome {
        struct HihContext {

            HihContext(uint16_t _height, uint16_t _width): height(_height), width(_width) {}

            uint16_t height;
            uint16_t width;
            std::mutex guard;
            volatile bool quit{false};

            void signal_quit() {
                const std::lock_guard<std::mutex> lock(this->guard);
                this->quit = true;
            }

            bool should_not_quit() {
                bool res = true;
                {
                    const std::lock_guard<std::mutex> lock(this->guard);
                    res = !this->quit;
                }

                return res;
            }
        };
        
    }

#endif
