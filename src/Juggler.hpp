#ifndef HIH_JUGLER_H
    #define HIH_JUGGLER_H
    
    #include <unistd.h>

    #include "DataTransfer.hpp"
    #include "HihContext.hpp"

    namespace honeyhome {
        class Juggler
        {
        private:
            DataTransfer * incoming_data;
            DataTransfer * outgoing_data;
            HihContext * context;

        public:
            Juggler(DataTransfer* _incoming,
                                DataTransfer* _outgoing,
                                HihContext* _context): incoming_data(_incoming),
                                                        outgoing_data(_outgoing),
                                                        context(_context) {
            }
            ~Juggler() {
            }
            void juggle() {
                while(!this->context->quit) {
                    {
                        void * buffer;
                        uint32_t size = 0;
                        size = outgoing_data->read_data(&buffer);
                        if (size < 1) {
                            usleep(1); // give it a tick
                            continue;
                        }
                        incoming_data->write_data(buffer, size);
                        free(buffer);
                    }
                }
            }
        };
    }
    

#endif
