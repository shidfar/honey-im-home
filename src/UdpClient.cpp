#include "UdpClient.hpp"

namespace honeyhome {

    UdpClient::UdpClient(HihContext * _context, DataTransfer * _outgoing): context(_context), outgoing_data(_outgoing) {
        std::cout << "Creating UDP client" << std::endl;
        memset(&this->server_addr, 0, sizeof(this->server_addr));
        
        // Filling server information 
        inet_pton(AF_INET, REMOTE_UDP_HOST, &(this->server_addr.sin_addr));
        this->server_addr.sin_family = AF_INET;
        this->server_addr.sin_port = htons(UDP_SERVER_PORT);
    }

    UdpClient::~UdpClient() {
        ;
    }

    bool UdpClient::initialize() {
        if ((this->socket_fd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
            std::cerr << "Could not create a socket" << std::endl;
            return false;
        }

        return true;
    }

    void UdpClient::sender_loop() {
        void *msg_buffer;
        int buff_len = -1;
        while (this->context->should_not_quit()) {
            if (this->outgoing_data->is_stale()) {
                usleep(1); // give it a tick
                continue;
            }
            buff_len = this->outgoing_data->read_data(&msg_buffer);
            // sendto(this->socket_fd, (const char *)"wazzaaap", 8, MSG_CONFIRM,
            //         (const struct sockaddr *) &this->server_addr, sizeof(this->server_addr));
            // usleep(FPS_24);
            sendto(this->socket_fd, (const char *)msg_buffer, buff_len, MSG_CONFIRM,
                    (const struct sockaddr *) &this->server_addr, sizeof(this->server_addr));
            
            if (buff_len > 0) {
                free(msg_buffer);
                buff_len = -1;
            }
        }
        sendto(this->socket_fd, (const char *)"die", 3, MSG_CONFIRM,
                    (const struct sockaddr *) &this->server_addr, sizeof(this->server_addr));
        close(this->socket_fd);
    }

}
