#ifndef HIH_UDP_CLIENT_H
    #define HIH_UDP_CLIENT_H

    #include <iostream>
    #include <cstdio>
    #include <cstdlib>
    #include <cstring>
    #include <sstream>

    #include <unistd.h>
    #include <sys/types.h>
    #include <sys/socket.h>
    #include <arpa/inet.h>
    #include <netinet/in.h>

    #include "HihContext.hpp"
    #include "DataTransfer.hpp"
    #include "GlobalDefs.h"

    namespace honeyhome {
        class UdpClient {
        private:
            int socket_fd;
            struct sockaddr_in server_addr;
            HihContext *context;
            DataTransfer *outgoing_data;
        public:
            UdpClient(HihContext *, DataTransfer *);
            ~UdpClient();
            bool initialize();
            void sender_loop();
        };
    }

#endif
