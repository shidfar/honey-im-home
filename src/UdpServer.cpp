#include "UdpServer.hpp"

namespace honeyhome {

    UdpServer::UdpServer(HihContext *_context, DataTransfer * _incoming): context(_context), incoming_data(_incoming) {
        std::cout << "Creating UDP Server" << std::endl;
        memset(&this->addr, 0, sizeof(this->addr));

        this->addr.sin_family = AF_INET;
        this->addr.sin_port = htons(UDP_SERVER_PORT);
        inet_pton(AF_INET, UDP_SERVER_HOST, &(this->addr.sin_addr));
    }

    UdpServer::~UdpServer() {
        ;
    }

    bool UdpServer::initialize() {
        if ((this->socket_fd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
            std::cerr << "Could not create a socket" << std::endl;
            return false;
        }
        
        if (bind(this->socket_fd, (const struct sockaddr *)&this->addr, sizeof(this->addr)) < 0) {
            std::cerr << "Bind failed" << std::endl;
            return false;
        }

        return true;
    }

    void UdpServer::reciever_loop() {
        struct sockaddr_in client_addr;
        socklen_t client_addr_len;
        int buff_len;
        char msg_buffer[UDP_PAYLOAD_SIZE]{0};
        char *hostaddrp;
        memset(&client_addr, 0, sizeof(client_addr));
        client_addr_len = sizeof(client_addr);

        while (this->context->should_not_quit()) {
            bzero(msg_buffer, UDP_PAYLOAD_SIZE);
            buff_len = recvfrom(this->socket_fd, (char *)msg_buffer,
                                UDP_PAYLOAD_SIZE, MSG_WAITALL,
                                (struct sockaddr *) &client_addr,
                                &client_addr_len);
            std::cout << " > Got a: " << buff_len << " bytes long message" << std::endl;
            this->incoming_data->write_data(msg_buffer, buff_len);
            
            hostaddrp = inet_ntoa(client_addr.sin_addr);
            std::cout << " > Recieved a frame from ";
            if (hostaddrp != NULL) std::cout << "hostaddrp: " << hostaddrp << std::endl;
            else std::cout << std::endl;
        }
        
    }

}
