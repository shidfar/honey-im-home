#ifndef HIH_UDP_SERVER_H
    #define HIH_UDP_SERVER_H

    #include <iostream>
    #include <cstdio>
    #include <cstdlib>
    #include <cstring>
    #include <sstream>
    #include <mutex>

    #include <unistd.h>
    #include <sys/types.h>
    #include <sys/socket.h>
    #include <arpa/inet.h>
    #include <netinet/in.h>

    #include "HihContext.hpp"
    #include "DataTransfer.hpp"
    #include "GlobalDefs.h"

    namespace honeyhome {
        class UdpServer
        {
        private:
            int socket_fd;
            struct sockaddr_in addr;
            HihContext *context;
            DataTransfer * incoming_data;

        public:
            UdpServer(HihContext *, DataTransfer *);
            ~UdpServer();
            bool initialize();
            void stay_listening();
            void reciever_loop();
        };
    }

#endif
