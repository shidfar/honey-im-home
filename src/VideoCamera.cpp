#include "VideoCamera.hpp"

namespace honeyhome {
    VideoCamera::VideoCamera(HihContext *_context, DataTransfer * _outgoing): context(_context), outgoing_data(_outgoing) {
        std::cout << "Creating VideoCamera instance" << std::endl;
        
        std::cout << "Starting the engine..." << std::endl;
        
        if(!this->open_vidocamera()) {
            std::cout << "Could not start camera..." << std::endl;
        }
    }
    
    VideoCamera::~VideoCamera() {
        std::cout << "Destroying VideoCamera instance" << std::endl;
        if (!this->close_videocamera()) {
            std::cout << "Could not gracefully shutdown the camera...";
        }
    }

    int VideoCamera::greetings() {
        std::cout << "Say hello to my little friend" << std::endl;
        return 0;
    }

    short VideoCamera::initialize() {
        if (this->get_device_capabilities()) {
            std::cout << "Successfully requested capabilities!" << std::endl;
            print_capabilities();
        } else return -1;

        if (this->can_stream()) {
            std::cout << "The device is capable of streaming!" << std::endl;
        } else return -1;

        print_available_formats();
        print_available_framesizes();

        if (prepare_device()) {
            std::cout << "Device prepared and ready to stream MJPEG at H:" 
                << this->context->height << " and W: " << this->context->width << std::endl;;
        } else return -1;

        if (request_buffer()) {
            std::cout << "Device Successfully informed about the buffer!" << std::endl;
        } else return -1;

        if (request_frame_memsize()) {
            std::cout << "Successfully requested frame memsize!" << std::endl;
        } else return -1;

        if (map_memory()) {
            std::cout << "Memory is mapped." << std::endl;
        } else return -1;
        
        return 0;
    }

    bool VideoCamera::open_vidocamera() {
        this->camera_fd = open("/dev/video0", O_RDWR);
        if (!this->is_camera_ok()) {
            std::cout << "Could not access camera..." << std::endl;
            return false;
        }
        return true;
    }

    bool VideoCamera::close_videocamera() {
        if (!this->is_camera_ok()) {
            std::cout << "Failed to close camera..." << std::endl;
            return false;
        }
        return true;
    }

    bool VideoCamera::is_camera_ok() {
        return !(this->camera_fd < 0);
    }

    bool VideoCamera::get_device_capabilities() {
        if(ioctl(this->camera_fd, VIDIOC_QUERYCAP, &this->cam_cap) < 0){
            perror("VIDIOC_QUERYCAP");
            return false;
        }
        return true;
    }

    bool VideoCamera::can_stream() {
        if (this->cam_cap.capabilities & V4L2_CAP_STREAMING) {
            return true;
        }
        return false;
    }

    bool VideoCamera::prepare_device() {
        std::cout << "Prepare the device for video capturing" << std::endl;
        this->video_format.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        this->video_format.fmt.pix.pixelformat = V4L2_PIX_FMT_MJPEG;
        this->video_format.fmt.pix.width = this->context->width;
        this->video_format.fmt.pix.height = this->context->height;

        
        if(ioctl(this->camera_fd, VIDIOC_S_FMT, &video_format) < 0) {
            std::cout << "Failed to prepare the device..." << std::endl;;
            perror("VIDIOC_S_FMT");
            return false;
        }

        return true;
    }

    bool VideoCamera::request_buffer() {
        std::cout << "Informing device about the buffer we're about to use..." << std::endl;
        struct v4l2_requestbuffers bufrequest;
        bufrequest.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        bufrequest.memory = V4L2_MEMORY_MMAP;
        bufrequest.count = 1;
        
        if(ioctl(this->camera_fd, VIDIOC_REQBUFS, &bufrequest) < 0) {
            std::cout << "Could not inform the device..." << std::endl;
            perror("VIDIOC_REQBUFS");
            return false;
        }

        return true;
    }

    bool VideoCamera::request_frame_memsize() {
        std::cout << "Requesting frame memsize." << std::endl;
        memset(&this->bufferinfo, 0, sizeof(this->bufferinfo));
        
        this->bufferinfo.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        this->bufferinfo.memory = V4L2_MEMORY_MMAP;
        this->bufferinfo.index = 0;
        
        if(ioctl(this->camera_fd, VIDIOC_QUERYBUF, &this->bufferinfo) < 0) {
            std::cout << "Failed to request frame size." << std::endl;
            perror("VIDIOC_QUERYBUF");
            return false;
        }
        return true;
    }

    bool VideoCamera::map_memory() {
        std::cout << "Setting up memory map." << std::endl;
        this->buffer_start = mmap(
            NULL,
            this->bufferinfo.length,
            PROT_READ | PROT_WRITE,
            MAP_SHARED,
            this->camera_fd,
            this->bufferinfo.m.offset
        );
        
        if(this->buffer_start == MAP_FAILED) {
            std::cout << "Could not create a memory map." << std::endl;
            perror("mmap");
            return false;
        }
        
        memset(this->buffer_start, 0, this->bufferinfo.length);

        return true;
    }

    void VideoCamera::start_streaming() {
        memset(&this->bufferinfo, 0, sizeof(this->bufferinfo));
 
        this->bufferinfo.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        this->bufferinfo.memory = V4L2_MEMORY_MMAP;
        this->bufferinfo.index = 0; /* Queueing buffer index 0. */
        
        // Activate streaming
        int type = this->bufferinfo.type;
        if(ioctl(this->camera_fd, VIDIOC_STREAMON, &type) < 0){
            perror("VIDIOC_STREAMON");
            return;
        }
        
        /* Here is where you typically start two loops:
        * - One which runs for as long as you want to
        *   capture frames (shoot the video).
        * - One which iterates over your buffers everytime. */
        while (this->context->should_not_quit()) {

            if(ioctl(this->camera_fd, VIDIOC_QBUF, &this->bufferinfo) < 0) {
                perror("VIDIOC_QBUF");
                this->context->signal_quit();
                break;
            }

            // The buffer's waiting in the outgoing queue.
            if(ioctl(this->camera_fd, VIDIOC_DQBUF, &this->bufferinfo) < 0) {
                perror("VIDIOC_QBUF");
                this->context->signal_quit();
                break;
            }

            this->outgoing_data->write_data(this->buffer_start, this->bufferinfo.length);
            // cout << "frame size: " << this->bufferinfo.bytesused / 1024 << "kB" << endl;
            usleep(FPS_24);
        }

        // Deactivate streaming
        if(ioctl(this->camera_fd, VIDIOC_STREAMOFF, &type) < 0) {
            perror("VIDIOC_STREAMOFF");
            return;
        }
    }

    void VideoCamera::print_capabilities() {
        std::map<int, std::string> capabilities_map = {
            { V4L2_CAP_VIDEO_CAPTURE, "V4L2_CAP_VIDEO_CAPTURE" },
            { V4L2_CAP_VIDEO_CAPTURE_MPLANE, "V4L2_CAP_VIDEO_CAPTURE_MPLANE" },
            { V4L2_CAP_VIDEO_OUTPUT, "V4L2_CAP_VIDEO_OUTPUT" },
            { V4L2_CAP_VIDEO_OUTPUT_MPLANE, "V4L2_CAP_VIDEO_OUTPUT_MPLANE" },
            { V4L2_CAP_VIDEO_M2M, "V4L2_CAP_VIDEO_M2M" },
            { V4L2_CAP_VIDEO_M2M_MPLANE, "V4L2_CAP_VIDEO_M2M_MPLANE" },
            { V4L2_CAP_VIDEO_OVERLAY, "V4L2_CAP_VIDEO_OVERLAY" },
            { V4L2_CAP_VBI_CAPTURE, "V4L2_CAP_VBI_CAPTURE" },
            { V4L2_CAP_VBI_OUTPUT, "V4L2_CAP_VBI_OUTPUT" },
            { V4L2_CAP_SLICED_VBI_CAPTURE, "V4L2_CAP_SLICED_VBI_CAPTURE" },
            { V4L2_CAP_SLICED_VBI_OUTPUT, "V4L2_CAP_SLICED_VBI_OUTPUT" },
            { V4L2_CAP_RDS_CAPTURE, "V4L2_CAP_RDS_CAPTURE" },
            { V4L2_CAP_VIDEO_OUTPUT_OVERLAY, "V4L2_CAP_VIDEO_OUTPUT_OVERLAY" },
            { V4L2_CAP_HW_FREQ_SEEK, "V4L2_CAP_HW_FREQ_SEEK" },
            { V4L2_CAP_RDS_OUTPUT, "V4L2_CAP_RDS_OUTPUT" },
            { V4L2_CAP_TUNER, "V4L2_CAP_TUNER" },
            { V4L2_CAP_AUDIO, "V4L2_CAP_AUDIO" },
            { V4L2_CAP_RADIO, "V4L2_CAP_RADIO" },
            { V4L2_CAP_MODULATOR, "V4L2_CAP_MODULATOR" },
            { V4L2_CAP_SDR_CAPTURE, "V4L2_CAP_SDR_CAPTURE" },
            { V4L2_CAP_EXT_PIX_FORMAT, "V4L2_CAP_EXT_PIX_FORMAT" },
            { V4L2_CAP_SDR_OUTPUT, "V4L2_CAP_SDR_OUTPUT" },
            { V4L2_CAP_META_CAPTURE, "V4L2_CAP_META_CAPTURE" },
            { V4L2_CAP_READWRITE, "V4L2_CAP_READWRITE" },
            { V4L2_CAP_ASYNCIO, "V4L2_CAP_ASYNCIO" },
            { V4L2_CAP_STREAMING, "V4L2_CAP_STREAMING" },
            // { V4L2_CAP_META_OUTPUT, "V4L2_CAP_META_OUTPUT" },
            { V4L2_CAP_TOUCH, "V4L2_CAP_TOUCH" },
            // { V4L2_CAP_IO_MC, "V4L2_CAP_IO_MC" },
            { V4L2_CAP_DEVICE_CAPS, "V4L2_CAP_DEVICE_CAPS" }
        };

        std::cout << std::endl;
        std::cout << " ----------------------------------------------------------------------------------------------------- " << std::endl;
        std::cout << "                                    Device Capabilities" << std::endl;
        std::cout << "  https://linuxtv.org/downloads/v4l-dvb-apis/userspace-api/v4l/vidioc-querycap.html#c.v4l2_capability" << std::endl;
        std::cout << " ----------------------------------------------------------------------------------------------------- " << std::endl;
        std::cout << std::endl;
        for (auto i = capabilities_map.begin(); i != capabilities_map.end(); i++) {
            if (this->cam_cap.capabilities & i->first) {
                std::cout << " | "<< i->second << std::endl;
            }
        }
        std::cout << std::endl;
        std::cout << " ----------------------------------------------------------------------------------------------------- " << std::endl;
        std::cout << std::endl;
    }

    void VideoCamera::print_available_formats() {
        struct v4l2_fmtdesc fmtdesc;
        memset(&fmtdesc, 0, sizeof(fmtdesc));
        fmtdesc.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

        std::cout << std::endl;
        std::cout << " ---------------------------- " << std::endl;
        std::cout << "       Available Formats" << std::endl;
        std::cout << " ---------------------------- " << std::endl;
        std::cout << std::endl;

        while (ioctl(this->camera_fd, VIDIOC_ENUM_FMT, &fmtdesc) == 0) {
            std::cout << " | " << fmtdesc.description << std::endl;
            fmtdesc.index++;
        }
        std::cout << std::endl;
        std::cout << " ----------------------------- " << std::endl;
        std::cout << std::endl;
    }

    void VideoCamera::print_available_framesizes() {
        // ioctl(int fd, VIDIOC_ENUM_FRAMESIZES, struct v4l2_frmsizeenum *argp)
        struct v4l2_frmsizeenum frm_size;
        memset(&frm_size, 0, sizeof(frm_size));
        frm_size.pixel_format = this->pixel_format;
        frm_size.type = V4L2_FRMSIZE_TYPE_DISCRETE;
        std::cout << std::endl;
        std::cout << " ---------------------------- " << std::endl;
        std::cout << "       Available Framesizes" << std::endl;
        std::cout << " ---------------------------- " << std::endl;
        std::cout << std::endl;
        while (ioctl(this->camera_fd, VIDIOC_ENUM_FRAMESIZES, &frm_size) == 0) {
            std::cout << " | height: " << frm_size.discrete.height 
                << ", width: " << frm_size.discrete.width 
                << " requires: " 
                << double(frm_size.discrete.height * frm_size.discrete.width * 3) / double(1024)
                << " KB per frame" << std::endl;
            frm_size.index ++;
        }
        std::cout << std::endl;
        std::cout << " ----------------------------- " << std::endl;
        std::cout << std::endl;

    }

}
