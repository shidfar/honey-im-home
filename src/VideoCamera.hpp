#ifndef HIH_VIDEO_CAMERA_H
    #define HIH_VIDEO_CAMERA_H

    #include <iostream>
    #include <cstdlib>
    #include <cstdio>
    #include <fstream>
    #include <string>
    #include <cstring>
    #include <map>
    #include <bitset>

    #include <linux/ioctl.h>
    #include <linux/types.h>
    #include <linux/v4l2-common.h>
    #include <linux/v4l2-controls.h>
    #include <linux/videodev2.h>
    
    #include <fcntl.h>
    #include <unistd.h>
    #include <sys/ioctl.h>
    #include <sys/mman.h>
    #include <thread>
    #include <algorithm>

    #include "HihContext.hpp"
    #include "DataTransfer.hpp"
    #include "GlobalDefs.h"

    namespace honeyhome
    {
        class VideoCamera
        {
        private:
            HihContext * context;
            DataTransfer * outgoing_data;
            int camera_fd;
            struct v4l2_capability cam_cap;
            struct v4l2_format video_format;

            void * buffer_start;
            struct v4l2_buffer bufferinfo;

            uint32_t pixel_format = V4L2_PIX_FMT_MJPEG;

            bool open_vidocamera();
            bool close_videocamera();
            bool is_camera_ok();
            bool get_device_capabilities();
            bool prepare_device();
            bool request_buffer();
            bool request_frame_memsize();
            bool map_memory();

            bool can_stream();
            void print_capabilities();
            void print_available_formats();
            void print_available_framesizes();
            
        public:
            VideoCamera(HihContext *, DataTransfer *);
            ~VideoCamera();
            short initialize();
            void start_streaming();
            int greetings();

        };
    } // namespace honeyhome

#endif