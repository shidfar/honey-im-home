#include "Window.hpp"

namespace honeyhome {
    Window::Window(HihContext *_context, DataTransfer * _incoming): context(_context), incoming(_incoming) {}
    
    Window::~Window() {
        std::cout <<"Destroyed the window" << std::endl;
    }

    void Window::start_event_loop() {
        SDL_Event event;
        bool quit = false;
        while (!quit) {
            SDL_WaitEvent(&event);
            switch (event.type) {
                case SDL_QUIT:
                    quit = true;
                    this->context->signal_quit();
                    break;
            }
        }
    }

    void Window::draw() {
        SDL_Init(SDL_INIT_VIDEO);
        IMG_Init(IMG_INIT_JPG);
        this->window = SDL_CreateWindow(
            WINDOW_NAME, 
            SDL_WINDOWPOS_UNDEFINED,
            SDL_WINDOWPOS_UNDEFINED,
            W_WIDTH, 
            W_HEIGHT,
            0
        );
        this->renderer = SDL_CreateRenderer(window, -1, 0);
        
        SDL_RWops* buffer_stream;
        while (this->context->should_not_quit())
        {
            if (this->incoming->is_stale()) {
                usleep(1); // give it a tick
                continue;
            }
            
            { // image frame scope
                void * buff;
                uint32_t size = 0;
                size = this->incoming->read_data(&buff);
                if (size < 1) {
                    usleep(1); // give it a tick
                    continue;
                }
                buffer_stream = SDL_RWFromMem(buff, size);
                usleep(FPS_24);
                SDL_Surface * image = IMG_Load_RW(buffer_stream, 0);
                SDL_Texture * texture = SDL_CreateTextureFromSurface(renderer, image);
                SDL_FreeSurface(image);
                SDL_RenderCopy(renderer, texture, NULL, NULL);
                SDL_DestroyTexture(texture);
                SDL_RenderPresent(renderer);
                free(buff);
            }
        }

        std::cout << "Closing the window" << std::endl;
        SDL_RWclose(buffer_stream);
        SDL_DestroyRenderer(renderer);
        SDL_DestroyWindow(window);
        std::cout << "Quiting the rest" << std::endl;
        IMG_Quit();
        SDL_Quit();
    }
}
