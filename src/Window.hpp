#ifndef HIH_WINDOW_H
    #define HIH_WINDOW_H

    #include <iostream>
    #include <thread>
    #include <unistd.h>

    #include <SDL2/SDL.h>
    #include <SDL2/SDL_image.h>
    #include <SDL2/SDL_timer.h> 

    #include "DataTransfer.hpp"
    #include "HihContext.hpp"
    #include "GlobalDefs.h"

    namespace honeyhome {
        class Window
        {
        private:
            HihContext *context;
            DataTransfer *incoming;
            SDL_Window * window{0};
            SDL_Renderer * renderer{0};
            
        public:
            Window(HihContext *, DataTransfer *);
            ~Window();
            void start_event_loop();
            void draw();
        };
    }

#endif
