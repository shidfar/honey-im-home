#include "ExecutionEngine.hpp"

int main() {
    honeyhome::ExecutionEngine *ee = new honeyhome::ExecutionEngine();

    ee->start();

    delete ee;

    return EXIT_SUCCESS;
}
